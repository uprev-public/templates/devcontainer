ARG BASE_IMAGE=uprev/base
ARG BASE_TAG=ubuntu-22.04
FROM ${BASE_IMAGE}:${BASE_TAG}


RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \ 
    #packages here 
    packages-to-install
    && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* 